import React from 'react';
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import axios from 'axios'
import axiosMiddleware from 'redux-axios-middleware'

import RootStack from './routes'
import reducer from './src/reducer'

const client = axios.create({
  baseURL: 'https://api.github.com',
  responseType: 'json'
})

const store = createStore(reducer, applyMiddleware(axiosMiddleware(client)))


export default class App extends React.Component {


  render() {
    return (
      <Provider store={store}>
        <RootStack />
      </Provider>
      )
  }
}
