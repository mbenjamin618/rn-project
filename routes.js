import React from 'react';
import { StackNavigator } from 'react-navigation'

import HomeScreen from './src/container/Home'
import DetailsScreen from './src/container/Detail'
import FlextypeScreen from './src/container/FlextypeScreen'

const RootStack = StackNavigator(
  {
    Home: {
      screen: HomeScreen,
    },
    Details: {
      screen: DetailsScreen,
    },
    Flextype: {
      screen: FlextypeScreen 
    }
  },
  {
    initialRouteName: 'Home',
  }
);

export default RootStack
