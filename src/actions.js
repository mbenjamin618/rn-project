import React from 'react'
import { GET_REPOS } from './constants'

export const listRepos = (user) => ({
  type: GET_REPOS,
  payload: {
    request: {
      url: `/users/${user}/repos`
    }
  }
})

export default { listRepos }