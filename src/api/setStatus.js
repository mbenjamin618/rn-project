// setStatus.js
// import {AsyncStorage} from 'react-native';

const API_URL = 'https://df6fc1de.ngrok.io'
const setStatus = async (currentState) => {

  try {
    // let token = await AsyncStorage.getItem('userToken');
    let response = await fetch(API_URL + '/hack/status/person1', {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        // Authorization: 'Token ' + token,
      },
      body: JSON.stringify({currentState}),
    });
    let json = await response.json();
    console.log('resp json', json);
    return json;
  } catch (error) {
    console.error(error) 
  }

};

export default setStatus;
