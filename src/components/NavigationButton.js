import React from 'react';
import { View, Text, Button } from 'react-native';


export default (props) => {
    return (
      <Button
        title={props.flextype || ''}
        onPress={() => this.props.navigation.navigate('Flextype', {
          flextype: props.flextype 
        })}
      />

    )
}
