import React from 'react';
import {Image, View, ScrollView, StyleSheet, Textinput} from 'react-native';
import {Button, CheckBox, Input, ListItem, Text} from 'react-native-elements';
import {FontAwesome, Feather, Ionicons} from '@expo/vector-icons';

import setStatus from '../api/setStatus';

class FlextypeScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      botherChecked: false,
      cellChecked: false,
      emailChecked: false,
      otherReachMeChecked: false,
      noVideoCallsChecked: false,
      noPhoneCallsChecked: false,
      otherLimitationsChecked: false,
      pingChecked: false,
      flexStatus: null,
      flexDetails: null,
    };
  }
  render() {
    return (
      <ScrollView contentContainerStyle={styles.container}>
        <View>
          <Image
            source={require('../../assets/appHeader.png')}
            style={styles.image}
          />
          <Image
            source={require('../../assets/deskplusX.png')}
            onPress={() => this.props.navigation.goBack()}
            style={styles.image}
          />
        </View>

        <View>
          <View style={styles.row}>
            <View style={styles.day}>
              <Image
                source={require('../../assets/daysFull.png')}
                style={styles.image}
              />
            </View>

            <View style={styles.day}>
              <Image
                source={require('../../assets/daysPart.png')}
                style={styles.image}
              />
            </View>
          </View>
        </View>

        <View>
          <View style={styles.timeInputs}>
            <View flex={2}>
              <Text>from</Text>
              <Input
                placeholder="HH:MM"
                containerStyle={{height: 42}}
                inputContainerStyle={{height: 40}}
              />
            </View>

            <View flex={1}>
              <Text>--</Text>
            </View>

            <View flex={2}>
              <Text>to</Text>
              <Input
                placeholder="HH:MM"
                containerStyle={{height: 42}}
                inputContainerStyle={{height: 40}}
              />
            </View>
          </View>
        </View>
        <View>
          <CheckBox
            title="DON'T FUCKING BOTHER ME 🤣"
            checked={this.state.botherChecked}
            containerStyle={styles.bgCheckboxContainer}
            onPress={() =>
              this.setState({botherChecked: !this.state.botherChecked})
            }
          />
        </View>
        <View>
          <View style={styles.sectionHeader}>
            <Text h4>BEST WAY TO REACH ME:</Text>
          </View>
          <View style={styles.checkboxes}>
            <CheckBox
              title="CELL 212-867-5309"
              checked={this.state.cellChecked}
              onPress={() =>
                this.setState({cellChecked: !this.state.cellChecked})
              }
            />
          </View>
          <View>
            <CheckBox
              title="EMAIL: LINDSAY@WERK.CO"
              checked={this.state.emailChecked}
              onPress={() =>
                this.setState({emailChecked: !this.state.emailChecked})
              }
            />
          </View>
          <View>
            <CheckBox
              title="OTHER"
              checked={this.state.otherReachMeChecked}
              onPress={() =>
                this.setState({
                  otherReachMeChecked: !this.state.otherReachMeChecked,
                })
              }
            />
          </View>
        </View>

        <View style={styles.sectionHeader}>
          <Text h4>LIMITATIONS:</Text>
        </View>

        <View>
          <View style={styles.row}>
            <Feather name="video-off" size={20} color="blue" />
            <View style={styles.checkboxes}>
              <CheckBox
                title="NO VIDEO CALLS"
                checked={this.state.noVideoCallsChecked}
                onPress={() =>
                  this.setState({
                    noVideoCallsChecked: !this.state.noVideoCallsChecked,
                  })
                }
              />
            </View>
          </View>
          <View>
            <View style={styles.row}>
              <Feather name="phone-off" size={20} color="blue" />
              <CheckBox
                title="NO PHONE CALLS"
                checked={this.state.noPhoneCallsChecked}
                onPress={() =>
                  this.setState({
                    noPhoneCallsChecked: !this.state.noPhoneCallsChecked,
                  })
                }
              />
            </View>
          </View>
          <View>
            <CheckBox
              title="OTHER"
              checked={this.state.otherChecked}
              onPress={() =>
                this.setState({otherChecked: !this.state.otherChecked})
              }
            />
          </View>
          <CheckBox
            title="Ping My Team"
            checked={this.state.pingChecked}
            containerStyle={styles.bgCheckboxContainer}
            onPress={() =>
              this.setState({
                pingChecked: !this.state.pingChecked,
              })
            }
          />
        </View>
        <View>
          <View style={styles.bottomButtons}>
            <Button
              buttonStyle={{
                backgroundColor: 'grey',
              }}
              icon={<FontAwesome name="plus" size={15} color="white" />}
              iconRight
              title="ADD"
            />
            <Button
              buttonStyle={{
                backgroundColor: '#5B6EA0',
              }}
              iconRight
              title="FLEX IT 💪"
              onPress={() => setStatus(this.state)}
            />
          </View>
        </View>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F6F3EF',
    paddingVertical: 20,
  },
  image: {
    resizeMode: 'contain',
    width: '100%',
  },
  sectionHeader: {
    alignItems: 'center',
  },
  timeInputs: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  checkboxes: {
    justifyContent: 'flex-start',
  },
  bottomButtons: {
    flexDirection: 'row',
    alignItems: 'stretch',
  },
  row: {
    flexDirection: 'row',
    flex: 1,
  },
  day: {
    flex: 1,
  },
  bgCheckboxContainer: {
    backgroundColor: '#F6F3EF',
  },
});
export default FlextypeScreen;
