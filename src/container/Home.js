import React from 'react';
import {Button, Image, View, Text, StyleSheet} from 'react-native';
// import NavigationButton from '../components/NavigationButton'
import Swipeable from 'react-native-swipeable';

class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };
  render() {
    return (
      <View style={styles.container}>

        <View style={styles.container}>
          <View stlye={styles.logo}>
            <Image
              source={require('../../assets/logos/3-260x93.png')}
              style={styles.image}
            />
          </View>
        </View>

        <View style={styles.container}>
          <Swipeable
            style={styles.swipeableContainer}
            rightContent={
              <View style={styles.rightSwipeItem}>
                <Text>Flex It</Text>
              </View>
            }
            onRightActionRelease={() =>
              this.props.navigation.navigate('Flextype', {
                flextype: 'microagility',
              })
            }>
            <View style={styles.listItem}>
              <Image
                source={require('../../assets/deskplusSwipe.png')}
                style={styles.swipeImage}
              />
            </View>
          </Swipeable>
          <Swipeable
            style={styles.swipeableContainer}
            rightContent={
              <View style={styles.rightSwipeItem}>
                <Text>Flex It</Text>
              </View>
            }
            onRightActionRelease={() =>
              this.props.navigation.navigate('Flextype', {
                flextype: 'microagility',
              })
            }>
            <View style={styles.listItem}>
              <Image
                source={require('../../assets/timeshiftSwipe.png')}
                style={styles.swipeImage}
              />
            </View>
          </Swipeable>
          <Swipeable
            style={styles.swipeableContainer}
            rightContent={
              <View style={styles.rightSwipeItem}>
                <Text>Flex It</Text>
              </View>
            }
            onRightActionRelease={() =>
              this.props.navigation.navigate('Flextype', {
                flextype: 'microagility',
              })
            }>
            <View style={styles.listItem}>
              <Image
                source={require('../../assets/microagilitySwipe.png')}
                style={styles.swipeImage}
              />
            </View>
          </Swipeable>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  listItem: {
    alignItems: 'center',
  },
  rightSwipeItem: {
    flex: 1,
    justifyContent: 'center',
    paddingLeft: 20,
    backgroundColor: '#5a6d9f',
  },
  swipeImage: {
    resizeMode: 'stretch',
    height: '100%',
    width: '100%',
  },
  swipeableContainer: {
    flex: 1,
  },
  logo: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    resizeMode: 'contain',
    width: '75%',
    height: '100%',

  },
});

export default HomeScreen;
