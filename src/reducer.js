import React from 'react'
import * as c from './constants'

export default function reducer(state = { repos: [] }, action) {
  switch (action.type) {
    case c.GET_REPOS:
      return { ...state, loading: true };
    case c.GET_REPOS_SUCCESS:
      return { ...state, loading: false, repos: action.payload.data };
    case c.GET_REPOS_FAIL:
      return { ...state, loading: false, error: action.payload.error };
    default:
      return state;
  }
}
